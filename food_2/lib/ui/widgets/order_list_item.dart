part of 'widgets.dart';

class OrderListItem extends StatelessWidget {
  final Transaction transaction;
  final double itemWidth;

  OrderListItem({this.transaction, this.itemWidth});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        //img
        Container(
          height: 60,
          width: 60,
          margin: EdgeInsets.only(right: 12),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(transaction.food.picturePath),
                  fit: BoxFit.cover)),
        ),
        SizedBox(
          width: itemWidth - 60 - 12 - 110,
          child: Column(
            children: [
              Text(
                transaction.food.name,
                style: blackTextFont3.copyWith(fontSize: 16),
                overflow: TextOverflow.clip,
              ),
              Text(
                '${transaction.quantity} item ' +
                    NumberFormat.currency(
                            locale: 'id-ID', symbol: 'IDR ', decimalDigits: 0)
                        .format(transaction.total),
                style: greyFontStyle.copyWith(fontSize: 13),
              )
            ],
          ),
        ),
        //tgl bulan dll
        SizedBox(
          width: 110,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                convertDateTime(transaction.dateTime),
                style: greyFontStyle.copyWith(fontSize: 12),
              ),
              (transaction.status == TransactionStatus.canceled)
                  ? Text(
                      'canceled',
                      style: greyFontStyle.copyWith(
                          fontSize: 10, color: 'd9435e'.toColor()),
                    )
                  : (transaction.status == TransactionStatus.pending)
                      ? Text(
                          'pending',
                          style: greyFontStyle.copyWith(
                              fontSize: 10, color: 'd94352'.toColor()),
                        )
                      : (transaction.status == TransactionStatus.on_delivery)
                          ? Text('on delivery',
                              style: greyFontStyle.copyWith(
                                  fontSize: 10, color: '1abc9c'.toColor()))
                          : SizedBox()
            ],
          ),
        )
      ],
    );
  }
}

String convertDateTime(DateTime dateTime) {
  String month;

  switch (dateTime.month) {
    case 1:
      month = 'jan';
      break;
    case 2:
      month = 'feb';
      break;
    case 3:
      month = 'mar';
      break;
    case 4:
      month = 'apr';
      break;
    case 5:
      month = 'may';
      break;
    case 6:
      month = 'jun';
      break;
    case 7:
      month = 'jul';
      break;
    case 8:
      month = 'aug';
      break;
    case 9:
      month = 'sept';
      break;
    case 10:
      month = 'oct';
      break;
    case 11:
      month = 'nov';
      break;
    default:
      month = 'des';
  }
  return month + '${dateTime.day} , ${dateTime.hour} : ${dateTime.minute}';
}
