part of 'widgets.dart';

class CustomTabbar extends StatelessWidget {
  final int selectedIndex;
  final List<String> title; //buat di food page
  final Function(int index) onTap; //buat ontap di food page

  CustomTabbar({this.selectedIndex, @required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: Stack(
        children: [
          //garis abu abu bawah
          Container(
            padding: EdgeInsets.only(top: 48),
            height: 1,
            color: 'f2f2f2'.toColor(),
          ),
          Row(
              children: title
                  .map((e) => Padding(
                        padding: EdgeInsets.only(left: defaultMargin),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            GestureDetector(
                              onTap: () {
                                if (onTap != null) {
                                  onTap(title.indexOf(e));
                                }
                              },
                              child: Text(
                                e,
                                style: (title.indexOf(e) == selectedIndex)
                                    ? blackTextFont3.copyWith(
                                        fontWeight: FontWeight.w500)
                                    : greyFontStyle,
                              ),
                            ),
                            //garis hitam
                            Container(
                              width: 40,
                              height: 3,
                              margin: EdgeInsets.only(top: 13),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(1.5),
                                  color: (title.indexOf(e) == selectedIndex)
                                      ? '020202'.toColor()
                                      : Colors.transparent),
                            )
                          ],
                        ),
                      ))
                  .toList())
        ],
      ),
    );
  }
}
