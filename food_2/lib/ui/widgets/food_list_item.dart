part of 'widgets.dart';

class FoodListItem extends StatelessWidget {
  final Food food;
  final double itemWidth;

  FoodListItem({@required this.food, this.itemWidth});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 60,
          width: 60,
          margin: EdgeInsets.only(right: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                  image: NetworkImage(food.picturePath), fit: BoxFit.cover)),
        ),
        SizedBox(
          width: itemWidth - 60 - 12 - 110,
          child: Column(
            children: [
              Text(
                food.name,
                style: blackTextFont2.copyWith(fontWeight: FontWeight.w400),
              ),
              Text(
                food.price.toString(),
                style: greyFontStyle.copyWith(fontSize: 13),
              )
            ],
          ),
        ),
        //ukuran width dari rating stars 110
        RatingStars(food.rate)
      ],
    );
  }
}
