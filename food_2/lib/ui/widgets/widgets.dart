import 'package:flutter/material.dart';
import 'package:food_2/models/models.dart';
import 'package:food_2/shared/shared.dart';
import 'package:intl/intl.dart';
// import 'package:get/get.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:supercharged/supercharged.dart';

part 'custom_bottom_navbar.dart';
part 'food_card.dart';
part 'rating_stars.dart';
part 'custom_tab_bar.dart';
part 'food_list_item.dart';
part 'order_list_item.dart';
