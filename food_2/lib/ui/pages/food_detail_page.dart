part of 'pages.dart';

class FoodDetailPage extends StatefulWidget {
  final Function onBackButton;
  final Transaction transaction;

  FoodDetailPage({this.onBackButton, this.transaction});

  @override
  _FoodDetailPageState createState() => _FoodDetailPageState();
}

class _FoodDetailPageState extends State<FoodDetailPage> {
  int quantity = 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: mainColor,
          ),
          SafeArea(
              child: Container(
            color: Colors.white,
          )),
          SafeArea(
              child: Container(
            height: 332,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(widget.transaction.food.picturePath),
                    fit: BoxFit.cover)),
          )),
          SafeArea(
              child: ListView(
            children: [
              Column(
                children: [
                  //icon
                  Container(
                    height: 100,
                    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          if (widget.onBackButton != null) {
                            widget.onBackButton();
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(3),
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.black12,
                              image: DecorationImage(
                                  image:
                                      AssetImage('assets/back_arrow_white.png'),
                                  fit: BoxFit.cover)),
                        ),
                      ),
                    ),
                  ),
                  //body
                  Container(
                    margin: EdgeInsets.only(top: 232),
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 26),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        color: Colors.black12),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  width: MediaQuery.of(context).size.width -
                                      32 -
                                      102,
                                  child: Text(
                                    widget.transaction.food.name,
                                    style: blackTextFont2.copyWith(
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                RatingStars(widget.transaction.food.rate)
                              ],
                            ),
                            Row(
                              children: [
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      //logika nya kalau mengurangi barang max sampai a/1 ,lalu dikurangi -1
                                      quantity = max(1, quantity - 1);
                                    });
                                  },
                                  child: Container(
                                      height: 26,
                                      width: 26,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'assets/btn_min.png'),
                                            fit: BoxFit.cover),
                                      )),
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 10),
                                  child: Text(
                                    quantity.toString(),
                                    style: blackTextFont2.copyWith(
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      //logika nya kalau beli barang = tambah
                                      //nah min pembelian sampai  a/999 barang lalau b/quantity+1
                                      quantity = min(999, quantity + 1);
                                    });
                                  },
                                  child: Container(
                                      height: 26,
                                      width: 26,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'assets/btn_add.png'),
                                            fit: BoxFit.cover),
                                      )),
                                )
                              ],
                            )
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 12, bottom: 16),
                          child: Text(widget.transaction.food.description,
                              style: greyFontStyle.copyWith(
                                  fontWeight: FontWeight.w400)),
                        ),
                        Text(
                          'Ingredients:',
                          style: blackTextFont3.copyWith(
                              fontWeight: FontWeight.w400),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 4, bottom: 40),
                          child: Text(
                            widget.transaction.food.ingredients,
                            style: greyFontStyle,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'total price',
                                  style: greyFontStyle.copyWith(fontSize: 13),
                                ),
                                Text(NumberFormat.currency(
                                        locale: 'id-ID',
                                        symbol: 'IDR',
                                        decimalDigits: 0)
                                    .format(quantity *
                                        widget.transaction.food.price))
                              ],
                            ),
                            Container(
                              height: 45,
                              width: 163,
                              child: ElevatedButton(
                                onPressed: () {
                                  Get.to(PaymentPage(
                                      transaction: widget.transaction.copywith(
                                          quantity: quantity,
                                          total: quantity *
                                              widget.transaction.food.price)));
                                },
                                child: Text(
                                  'order now',
                                  style: blackTextFont3,
                                ),
                                style: ElevatedButton.styleFrom(
                                    primary: mainColor,
                                    elevation: 0,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(8))),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ))
        ],
      ),
    );
  }
}
