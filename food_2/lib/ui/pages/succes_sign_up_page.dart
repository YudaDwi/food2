part of 'pages.dart';

class SuccesSignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IllustrationPage(
        title: "yeay! completed",
        subtitle: 'now you are able to order\nsome foods as a self-reward',
        picturePath: 'assets/food_wishes.png',
        buttonTitle1: 'find foods',
        buttonTap1: () {},
      ),
    );
  }
}
