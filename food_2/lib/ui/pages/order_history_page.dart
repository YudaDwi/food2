part of 'pages.dart';

class OrderHistoryPage extends StatefulWidget {
  @override
  _OrderHistoryPageState createState() => _OrderHistoryPageState();
}

class _OrderHistoryPageState extends State<OrderHistoryPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    //kalau inprogress dan past nya itu kosong maka akan mengembalikan
    return BlocBuilder<TransactionCubit, TransactionState>(builder: (_, state) {
      if (state is TransactionLoaded) {
        //kondisinya dirubah
        if (state.transaction.length == 0) {
          return IllustrationPage(
              title: 'ouch hungry',
              subtitle: 'seems you like have not\nordered any food yet',
              picturePath: 'assets/love_burger.png',
              buttonTitle1: 'find foods',
              buttonTap1: () {});
        } else {
          double listItemWidth =
              MediaQuery.of(context).size.width - 2 * defaultMargin;
          return GeneralPage(
            title: 'your orders',
            subtitle: 'wait for the best meal',
            child: Container(
              color: Colors.white,
              width: double.infinity,
              child: Column(
                children: [
                  CustomTabbar(
                    title: ['in progress', 'past order'],
                    selectedIndex: selectedIndex,
                    onTap: (index) {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  //ditandai ditambah builder
                  Builder(builder: (_) {
                    List<Transaction> transaction = (selectedIndex == 0)
                        ? state.transaction
                            .where((element) =>
                                element.status ==
                                    TransactionStatus.on_delivery ||
                                element.status == TransactionStatus.pending)
                            .toList()
                        : state.transaction
                            .where((element) =>
                                element.status == TransactionStatus.delivered ||
                                element.status == TransactionStatus.canceled)
                            .toList();
                    return Column(
                        children: (transaction)
                            .map((e) => Padding(
                                  padding: EdgeInsets.only(
                                      left: defaultMargin,
                                      right: defaultMargin,
                                      bottom: 16),
                                  child: OrderListItem(
                                    transaction: e,
                                    itemWidth: listItemWidth,
                                  ),
                                ))
                            .toList());
                  })
                ],
              ),
            ),
          );
        }
      } else {
        return Center(
          child: loadingIndicator,
        );
      }
    });
  }
}
