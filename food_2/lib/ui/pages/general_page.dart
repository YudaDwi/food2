part of 'pages.dart';

class GeneralPage extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function onBackButton;
  final Widget child;
  final Color backColor;

  GeneralPage(
      {this.title = 'title',
      this.subtitle = 'subtitle',
      this.onBackButton,
      this.child,
      this.backColor});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(color: Colors.white),
        SafeArea(
            child: Container(
                //backcolor kalau dia ada isinya kalau null maka akan mengembalikan nilai fafafc
                color: backColor ?? Colors.white)),
        SafeArea(
          child: ListView(
            children: [
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                    width: double.infinity,
                    height: 108,
                    color: Colors.white,
                    child: Row(
                      children: [
                        onBackButton != null
                            ? GestureDetector(
                                onTap: () {
                                  if (onBackButton != null) {
                                    onBackButton();
                                  }
                                },
                                child: Container(
                                  height: 24,
                                  width: 24,
                                  margin: EdgeInsets.only(right: 26),
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(
                                              'assets/back_arrow.png'))),
                                ),
                              )
                            : SizedBox(),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              title,
                              style: GoogleFonts.poppins(
                                  fontSize: 22, fontWeight: FontWeight.w500),
                            ),
                            Text(
                              subtitle,
                              style: GoogleFonts.poppins(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w300,
                                  color: '8d92a3'.toColor()),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  //1.1 kalau child ada isinya kalau tidak ada diisi sizedbox
                  //child ini dari parameter
                  Container(
                    height: defaultMargin,
                    width: double.infinity,
                    color: 'fafafc'.toColor(),
                  ),
                  child ?? SizedBox()
                ],
              )
            ],
          ),
        )
      ],
    ));
  }
}
