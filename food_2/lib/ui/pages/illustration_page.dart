part of 'pages.dart';

class IllustrationPage extends StatelessWidget {
  final String title;
  final String subtitle;
  final String picturePath;
  final String buttonTitle1;
  final String buttonTitle2;
  final Function buttonTap1;
  final Function buttonTap2;

  IllustrationPage(
      {@required this.title,
      @required this.subtitle,
      @required this.picturePath,
      @required this.buttonTitle1,
      this.buttonTitle2,
      @required this.buttonTap1,
      this.buttonTap2});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //img
          Container(
            height: 150,
            width: 150,
            margin: EdgeInsets.only(bottom: 30),
            decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage(picturePath))),
          ),
          //title
          Text(
            title,
            style: blackTextFont1.copyWith(
                fontSize: 20, fontWeight: FontWeight.w400),
          ),
          SizedBox(
            height: 6,
          ),
          Text(
            subtitle,
            style: greyFontStyle.copyWith(fontWeight: FontWeight.w300),
            textAlign: TextAlign.center,
          ),
          Container(
            margin: EdgeInsets.only(top: 30, bottom: 12),
            height: 45,
            width: 200,
            child: ElevatedButton(
              onPressed: buttonTap1,
              child: Text(
                buttonTitle1,
                style: blackTextFont3,
              ),
              style: ElevatedButton.styleFrom(
                  elevation: 0,
                  primary: mainColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8))),
            ),
          ),
          (buttonTap2 == null)
              ? SizedBox()
              : Container(
                  margin: EdgeInsets.only(bottom: 12),
                  height: 45,
                  width: 200,
                  child: ElevatedButton(
                    onPressed: buttonTap2,
                    child: Text(
                      buttonTitle2,
                      style: blackTextFont3.copyWith(color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                        elevation: 0,
                        primary: greyColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8))),
                  ),
                ),
        ],
      ),
    );
  }
}
