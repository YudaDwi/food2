part of 'pages.dart';

class AddressPage extends StatefulWidget {
  final User user;
  final String password;
  final File pictureFile;

  AddressPage(this.user, this.password, this.pictureFile);

  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  TextEditingController addresslController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController houseController = TextEditingController();
  //5.4 ini menunjukan bahwa si address page ini sedang meng upload data user ke backedn atau tidak
  bool isLoading = false;
  List<String> cities;
  String selectedCity;

  @override
  void initState() {
    super.initState();
    cities = ['ponorogo', 'bandung', 'jakarta', 'madiun'];
    selectedCity = cities[0];
  }

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Address',
        subtitle: "make sure it's a valid",
        onBackButton: () {
          Get.back();
        },
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              width: double.infinity,
              child: Text(
                'Full Name',
                style: blackTextFont2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                  ),
                  borderRadius: BorderRadius.circular(8)),
              child: TextField(
                controller: phoneController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: 'type your phone number'),
              ),
            ),
            //emailaddress
            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
              width: double.infinity,
              child: Text(
                'Address',
                style: blackTextFont2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                controller: addresslController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: 'type your address'),
              ),
            ),

            //password
            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              width: double.infinity,
              child: Text(
                'House No.',
                style: blackTextFont2,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.black)),
              child: TextField(
                controller: houseController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintStyle: greyFontStyle,
                  hintText: 'Type Your Password',
                ),
              ),
            ),

            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              width: double.infinity,
              child: Text(
                'city',
                style: blackTextFont2,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.black)),
              child: DropdownButton(
                value: selectedCity,
                isExpanded: true,
                underline: SizedBox(),
                items: cities
                    .map(
                      (e) => DropdownMenuItem(
                          value: e,
                          child: Text(
                            e,
                            style: blackTextFont3,
                          )),
                    )
                    .toList(),
                onChanged: (item) {
                  setState(() {
                    selectedCity = item;
                  });
                },
              ),
            ),
            //button sign in
            Container(
              width: double.infinity,
              height: 45,
              margin: EdgeInsets.only(top: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: (isLoading == true)
                  ? Center(
                      child: loadingIndicator,
                    )
                  : ElevatedButton(
                      onPressed: () async {
                        User user = widget.user.copyWith(
                            phoneNum: phoneController.text,
                            address: addresslController.text,
                            houseNum: houseController.text,
                            city: selectedCity);

                        setState(() {
                          isLoading = true;
                        });

                        await context.bloc<UserCubit>().signUp(
                            user, widget.password,
                            pictureFile: widget.pictureFile);

                        //mengambil State saat ini
                        UserState state = context.bloc<UserCubit>().state;

                        //jika state saat ini adl userloaded artinya benar
                        if (state is UserLoaded) {
                          context.bloc<FoodCubit>().getFoods();
                          context.bloc<TransactionCubit>().getTransaction();
                          Get.to(MainPage());
                        } else {
                          Get.snackbar('', '',
                              backgroundColor: 'd9435e'.toColor(),
                              icon: Icon(
                                MdiIcons.closeCircleOutline,
                                color: Colors.white,
                              ),
                              titleText: Text(
                                'sign in failed',
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ),
                              messageText: Text(
                                  (state as UserLoadedFailed).message,
                                  style: GoogleFonts.poppins(
                                      color: Colors.white)));

                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      child: Text(
                        'Sign Up Now',
                        style: blackTextFont3,
                      ),
                      style: ElevatedButton.styleFrom(
                          primary: Colors.yellow,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8))),
                    ),
            ),
          ],
        ));
  }
}
