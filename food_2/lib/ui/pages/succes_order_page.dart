part of 'pages.dart';

class SuccesOrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IllustrationPage(
        title: "you've made order",
        subtitle: 'just stay at home while we are\npreparing your best foods',
        picturePath: 'assets/bike.png',
        buttonTitle1: 'order other foods',
        buttonTap1: () {
          Get.offAll(MainPage());
        },
        buttonTitle2: 'view my order',
        buttonTap2: () {
          Get.offAll(MainPage(
            initialPage: 1,
          ));
        },
      ),
    );
  }
}
