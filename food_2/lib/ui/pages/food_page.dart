part of 'pages.dart';

//karena food ini berubah ubah jadi pakai stf

class FoodPage extends StatefulWidget {
  @override
  _FoodPageState createState() => _FoodPageState();
}

class _FoodPageState extends State<FoodPage> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    double listItem = MediaQuery.of(context).size.width - 2 * defaultMargin;
    return ListView(
      children: [
        Column(
          children: [
            //header
            Container(
              height: 108,
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              margin: EdgeInsets.only(bottom: 12),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Food Market',
                        style: blackTextFont1,
                      ),
                      Text(
                        "let's get some foods",
                        style:
                            greyFontStyle.copyWith(fontWeight: FontWeight.w300),
                      )
                    ],
                  ),
                  Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            image: NetworkImage(
                                //karena sudah sign in jadi otomatis bisa langsung default gambar
                                //dan UserLoaded sudah benar karena sebelumnya di sign in sudah di cek
                                (context.bloc<UserCubit>().state as UserLoaded)
                                    .user
                                    .picturePath),
                            fit: BoxFit.cover)),
                  )
                ],
              ),
            ),
            //list of food 1
            Container(
              height: 258,
              width: double.infinity,
              //TODO masih penasaran fungsi bloc builder
              //berarti yg dibawah juga di bloc builder ??
              child: BlocBuilder<FoodCubit, FoodState>(
                  builder: (_, state) => (state is FoodLoaded)
                      ? ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                            Row(
                              //kalau sebelumnya diisi dari mockfood
                              //ini diisi langsung dri cubit state itu sendiri
                              children: state.food
                                  .map((e) => Padding(
                                        padding: EdgeInsets.only(
                                            left: (e == state.food.first)
                                                ? defaultMargin
                                                : 0,
                                            right: defaultMargin),
                                        child: GestureDetector(
                                            onTap: () {
                                              Get.to(FoodDetailPage(
                                                transaction: Transaction(
                                                  food: e,
                                                  user: (context
                                                          .bloc<UserCubit>()
                                                          .state as UserLoaded)
                                                      .user,
                                                ),
                                                onBackButton: () {
                                                  Get.back();
                                                },
                                              ));
                                            },
                                            child: FoodCard(e)),
                                      ))
                                  .toList(),
                            )
                          ],
                        )
                      : Center(
                          child: loadingIndicator,
                        )),
            ),
            //list of food 2 bg tab
            Container(
              width: double.infinity,
              color: Colors.white,
              child: Column(
                children: [
                  CustomTabbar(
                    title: ['new taste', 'populer', 'recomended'],
                    selectedIndex: selectedIndex,
                    onTap: (index) {
                      setState(() {
                        selectedIndex = index;
                      });
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  //TODO fungsi blocbuilder apakah sama dgn builder ??
                  BlocBuilder<FoodCubit, FoodState>(builder: (_, state) {
                    if (state is FoodLoaded) {
                      List<Food> foods = state.food
                          .where((element) =>
                              element.types.contains((selectedIndex == 0)
                                  ? FoodType.new_food
                                  : (selectedIndex == 1)
                                      ? FoodType.popular
                                      : FoodType.recomended))
                          .toList();

                      return Column(
                        children: foods
                            .map((e) => Padding(
                                  padding: EdgeInsets.fromLTRB(
                                      defaultMargin, 0, defaultMargin, 16),
                                  child: FoodListItem(
                                      food: e, itemWidth: listItem),
                                ))
                            .toList(),
                      );
                    } else {
                      return Center(
                        child: loadingIndicator,
                      );
                    }
                  }),
                  SizedBox(
                    height: 80,
                  )
                ],
              ),
            )
          ],
        )
      ],
    );
  }
}
