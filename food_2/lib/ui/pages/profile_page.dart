part of 'pages.dart';

class ProfilePage extends StatefulWidget {
  final User user;
  ProfilePage({this.user});
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int selecteIndex = 0;
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        //header
        Container(
          margin: EdgeInsets.only(bottom: defaultMargin),
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          height: 232,
          color: Colors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(bottom: 16),
                height: 90,
                width: 90,
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/photo_border.png'))),
                child: Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                              (context.bloc<UserCubit>().state as UserLoaded)
                                  .user
                                  .picturePath),
                          fit: BoxFit.cover)),
                ),
              ),
              Text(
                (context.bloc<UserCubit>().state as UserLoaded).user.name,
                style: blackTextFont2.copyWith(fontSize: 18),
              ),
              Container(
                margin: EdgeInsets.only(top: 6),
                child: Text(
                  (context.bloc<UserCubit>().state as UserLoaded).user.email,
                  style: greyFontStyle.copyWith(fontWeight: FontWeight.w300),
                ),
              )
            ],
          ),
        ),
        //body
        Container(
          color: Colors.white,
          padding:
              EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 16),
          width: double.infinity,
          child: Column(
            children: [
              CustomTabbar(
                title: ['Account', 'Food Market'],
                selectedIndex: selecteIndex,
                onTap: (index) {
                  setState(() {
                    selecteIndex = index;
                  });
                },
              ),
              SizedBox(
                height: 18,
              ),
              Column(
                children: ((selecteIndex == 0)
                        ? [
                            'Edit Profile',
                            'Home Address',
                            'Security',
                            'Payments'
                          ]
                        : [
                            'Rate App',
                            'Help Center',
                            'Privacy & Policy',
                            'Terms & Conditions'
                          ])
                    .map((e) => Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              e,
                              style: blackTextFont3,
                            ),
                            SizedBox(
                              height: 24,
                              width: 24,
                              child: Image.asset(
                                'assets/right_arrow.png',
                                fit: BoxFit.contain,
                              ),
                            )
                          ],
                        ))
                    .toList(),
              )
            ],
          ),
        )
      ],
    );
  }
}
