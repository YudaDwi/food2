part of 'pages.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isLoading = false;
  //1.2 isloading itu adalah ketika kita klik tombol sign in maka akan ada progres indikator

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Sign In',
        subtitle: 'find your best ever meal',
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
              width: double.infinity,
              child: Text(
                'Email Address',
                style: blackTextFont2,
              ),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: Colors.black),
              ),
              child: TextField(
                controller: emailController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: 'type your email address'),
              ),
            ),

            //password
            Container(
              margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
              width: double.infinity,
              child: Text(
                'Password',
                style: blackTextFont2,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: 10),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.black)),
              child: TextField(
                controller: passwordController,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintStyle: greyFontStyle,
                  hintText: 'Type Your Password',
                ),
              ),
            ),

            //button sign in
            Container(
              width: double.infinity,
              height: 45,
              margin: EdgeInsets.only(top: defaultMargin),
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: isLoading
                  ? SpinKitFadingCircle(
                      size: 45,
                      color: mainColor,
                    )
                  : ElevatedButton(
                      onPressed: () async {
                        setState(() {
                          //isloading diisi true karena memang sudah benar
                          isLoading = true;
                        });

                        await context.bloc<UserCubit>().signIn(
                            emailController.text, passwordController.text);

                        //mengambil State saat ini
                        UserState state = context.bloc<UserCubit>().state;

                        //jika state saat ini adl userloaded artinya benar
                        if (state is UserLoaded) {
                          context.bloc<FoodCubit>().getFoods();
                          context.bloc<TransactionCubit>().getTransaction();
                          Get.to(MainPage());
                        } else {
                          Get.snackbar('', '',
                              backgroundColor: 'd9435e'.toColor(),
                              icon: Icon(
                                MdiIcons.closeCircleOutline,
                                color: Colors.white,
                              ),
                              titleText: Text(
                                'sign in failed',
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600),
                              ),
                              messageText: Text(
                                  (state as UserLoadedFailed).message,
                                  style: GoogleFonts.poppins(
                                      color: Colors.white)));

                          setState(() {
                            isLoading = false;
                          });
                        }
                      },
                      child: Text(
                        'Sign In',
                        style: blackTextFont3,
                      ),
                      style: ElevatedButton.styleFrom(
                          primary: Colors.yellow,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8))),
                    ),
            ),
            //button signup /create new account
            Container(
              width: double.infinity,
              height: 45,
              margin: EdgeInsets.only(top: 12),
              padding: EdgeInsets.symmetric(horizontal: defaultMargin),
              child: isLoading
                  ? loadingIndicator
                  : ElevatedButton(
                      onPressed: () {
                        Get.to(SignUpPage());
                      },
                      child: Text(
                        'Create New Account',
                        style: blackTextFont3.copyWith(color: Colors.white),
                      ),
                      style: ElevatedButton.styleFrom(
                          primary: greyColor,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8))),
                    ),
            )
          ],
        ));
  }
}
