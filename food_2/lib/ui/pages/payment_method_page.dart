part of 'pages.dart';

class PaymentMethodPage extends StatelessWidget {
  final String paymentUrl;
  PaymentMethodPage(this.paymentUrl);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IllustrationPage(
        title: "Finish your payment",
        subtitle: 'please select your payment\npayment method',
        picturePath: 'assets/payment.png',
        buttonTitle1: 'payment method',
        buttonTap1: () async {
          await launch(paymentUrl);
        },
        buttonTitle2: 'continue',
        buttonTap2: () {
          Get.to(SuccesOrderPage());
        },
      ),
    );
  }
}
