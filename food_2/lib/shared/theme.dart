part of 'shared.dart';

Color mainColor = 'ffc700'.toColor();
Color greyColor = '8d92a3'.toColor();

TextStyle greyFontStyle = GoogleFonts.poppins().copyWith(color: greyColor);
TextStyle blackTextFont1 = GoogleFonts.poppins()
    .copyWith(fontSize: 22, fontWeight: FontWeight.w500, color: Colors.black);
TextStyle blackTextFont2 = GoogleFonts.poppins()
    .copyWith(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black);
TextStyle blackTextFont3 = GoogleFonts.poppins()
    .copyWith(fontSize: 14, fontWeight: FontWeight.w400, color: Colors.black);

const double defaultMargin = 24;

Widget loadingIndicator = SpinKitFadingCircle(
  size: 45,
  color: mainColor,
);
