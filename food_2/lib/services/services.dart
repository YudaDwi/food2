import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:food_2/models/models.dart';

part 'food_services.dart';
part 'transaction_services.dart';
part 'user_services.dart';

String baseUrl = 'http://foodmarket-backend.buildwithangga.id/api/';
