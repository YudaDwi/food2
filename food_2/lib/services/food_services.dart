part of 'services.dart';

class FoodServices {
  static Future<ApiReturnValue<List<Food>>> getFoods(
      {http.Client client}) async {
    //5.6 ini dicek kalau dia null maka akan diisi dgn httpclient yg baru
    client ??= http.Client();

    //buat url
    String url = baseUrl + 'food';

    var response = await client.get(url);

    //di cek,kalau status code salah/bukan 200 maka
    //retrun apiretrun value dgn message
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'please try again later');
    }

    //kalau benar
    var data = jsonDecode(response.body);

    //pengertian iterable belum paham
    List<Food> foods = (data['data']['data'] as Iterable)
        .map((e) => Food.fromJson(e))
        .toList();

    return ApiReturnValue(value: foods);
  }
}
