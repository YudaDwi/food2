part of 'services.dart';

class UserServices {
  static Future<ApiReturnValue<User>> signIn(String email, String password,
      {http.Client client}) async {
    if (client == null) {
      client = http.Client();
    }
    String url = baseUrl + 'login';

    var response = await client.post(url,
        headers: {"Content-Type": "application/json"},
        body:
            //5.8 mungkin karena ini cuma email dan password maka hanya diberi string key dan string value
            jsonEncode(<String, String>{'email': email, 'password': password}));
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'please try again later');
    }

    //kalau berhsil di bawah
    //di decode
    var data = jsonDecode(response.body);

    //data disini diambil dari data di atas karena sudah di decode dan diresponse ke body nya
    User.token = data['data']['access_token'];
    User value = User.fromJson(data['data']['user']);

    return ApiReturnValue(value: value);
  }

  //5.2 yg dibutuhkan disini user password dan opsional pictureFile
  //http diawal itu menunjukan as http yg diimport
  static Future<ApiReturnValue<User>> signUp(User user, String password,
      {File pictureFile, http.Client client}) async {
    //5.2 kalau client nya null kita kembalikan http client yg baru
    if (client == null) {
      client = http.Client();
    }
    String url = baseUrl + 'register';

    //5.2 karena ini tipenya post
    //sepertinya post nanti ada dua satu ini dan satunya submnit transaction
    //jsonEncode mengubah data array/nilai variabel menjadi json
    //json decode mengubah json menjadi data array/nilai variabel
    //dibuat jsonEncode dgn satu buah mapstring
    var response = await client.post(url,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(<String, String>{
          'name': user.name,
          'email': user.email,
          'password': password,
          'password_confirmation': password,
          'address': user.address,
          'city': user.city,
          'houseNumber': user.houseNum,
          'phoneNumber': user.phoneNum
        }));
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'please try again');
    }

    //kalau berhasil maka json nya akan di decode

    var data = jsonDecode(response.body);
    //yg pertama dilakukan adl menyimpan token yg dikembalikan

    User.token = data['data']['acces_token'];
    //lalu ini yg akan dikembalikan
    //ini berdasarkan web postman yg response register
    User value = User.fromJson(data['data']['user']);

    //TODO upload pp
    if (pictureFile != null) {
      ApiReturnValue<String> result = await uploadProfilePicture(pictureFile);
      if (result.value != null) {
        value = value.copyWith(
            picturePath:
                'http://foodmarket-backend.buildwithangga.id/api/storage/' +
                    result.value);
      }
    }

    return ApiReturnValue(value: value);
  }

  //kita menggunakan multipart request untuk meng upload profile picture kita
  //perlu diteliti lagi multipartreques
  static Future<ApiReturnValue<String>> uploadProfilePicture(File pictureFile,
      {http.MultipartRequest request}) async {
    String url = baseUrl + 'user/photo';
    var uri = Uri.parse(url);

    //ini untuk rquest
    if (request == null) {
      request = http.MultipartRequest("POST", uri)
        ..headers["Content-type"] = "application/json"
        ..headers["Authorization"] = "Bearer ${User.token}";
    }

    //belum paham maksud ini
    var multiPartFile =
        await http.MultipartFile.fromPath('file', pictureFile.path);
    request.files.add(multiPartFile);

    var response = await request.send();

    if (response.statusCode == 200) {
      String responseBody = await response.stream.bytesToString();
      var data = jsonDecode(responseBody);

      String imagePath = data['data'][0];
      return ApiReturnValue(value: imagePath);
    } else {
      return ApiReturnValue(message: 'uploading profile picture failed');
    }
  }
}

//url launcher untuk menampilkan yg dihasilkan dari midtrans
