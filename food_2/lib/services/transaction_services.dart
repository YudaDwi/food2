part of 'services.dart';

class TransactionServices {
  static Future<ApiReturnValue<List<Transaction>>> getTransaction(
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseUrl + 'transaction';

    var response = await client.get(url, headers: {
      "Content-Type": "application/json",
      "Authorization": "Bearer ${User.token}"
    });
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'please try again later');
    }
    var data = jsonDecode(response.body);

    //apa karena ini list jadi mungkin di mapping
    List<Transaction> transactions = (data['data']['data'] as Iterable)
        .map((e) => Transaction.fromJson(e))
        .toList();

    return ApiReturnValue(value: transactions);
  }

  static Future<ApiReturnValue<Transaction>> submitTransaction(
      Transaction transactions,
      {http.Client client}) async {
    client ??= http.Client();

    String url = baseUrl + 'checkout';

    var response = await client.post(url,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer ${User.token}"
        },
        body: jsonEncode(<String, dynamic>{
          'food_id': transactions.food.id,
          'user_id': transactions.user.id,
          'quantity': transactions.quantity,
          'total': transactions.total,
          //5.8 status nya dimasukan yaitu pending karena belum bayar
          'status': 'PENDING'
        }));
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'please try again');
    }

    var data = jsonDecode(response.body);

    Transaction value = Transaction.fromJson(data['data']);

    return ApiReturnValue(value: value);
  }
}
