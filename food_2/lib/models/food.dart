part of 'models.dart';

enum FoodType { new_food, popular, recomended }

class Food extends Equatable {
  final int id;
  final String picturePath;
  final String name;
  final String description;
  final String ingredients;
  final int price;
  final double rate;
  final List<FoodType> types;

  Food(
      {this.id,
      this.picturePath,
      this.name,
      this.description,
      this.ingredients,
      this.price,
      this.rate,
      this.types = const []});

  factory Food.fromJson(Map<String, dynamic> data) => Food(
      id: data['id'],
      picturePath: data['picturePath'],
      name: data['name'],
      description: data['description'],
      ingredients: data['ingredients'],
      price: data['price'],
      //5.6 num itu parent dari int dan double lalu diubah ke double
      rate: (data['rate'] as num).toDouble(),
      types: data['types'].toString().split(',').map((e) {
        switch (e) {
          case 'recomended':
            return FoodType.recomended;
            break;
          case 'Popular':
            return FoodType.popular;
            break;
          default:
            return FoodType.new_food;
        }
      }).toList());

  @override
  List<Object> get props =>
      [id, picturePath, name, description, ingredients, price, rate, types];
}

List<Food> mockFoods = [
  Food(
      id: 1,
      picturePath:
          'https://res.cloudinary.com/dk0z4ums3/image/upload/v1594169572/attached_image/daftar-makanan-tinggi-garam-yang-perlu-diwaspadai.jpg',
      name: 'sate sayur sultan',
      ingredients: 'bawang merah, paprika, bawang bombay, timun',
      description: 'sate sayur sultan adalah makanan khas untuk para sultan jongos dilarang',
      price: 150000,
      rate: 4.2,
      types: [FoodType.new_food, FoodType.recomended, FoodType.popular]),
  Food(
      id: 2,
      picturePath:
          'https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2020/02/makanan-korea-halal.jpg',
      name: 'lontong sultan',
      ingredients: 'lontong, bawang ,siomay, mangkok',
      description: 'lontong sultan adalah makanan para sultan yg hanya untuk sultan pada saat lebaran ',
      price: 250000,
      rate: 4,
      types: [FoodType.new_food]),
  Food(
      id: 3,
      picturePath:
          'https://res.cloudinary.com/dk0z4ums3/image/upload/v1594169572/attached_image/daftar-makanan-tinggi-garam-yang-perlu-diwaspadai.jpg',
      name: 'rujak sultan',
      ingredients: 'naga emas, bunga lili, kacang legend, micin import',
      description: 'makanan yg bahan bahanya diimpor langsung dari luar negri',
      price: 350000,
      rate: 3.5,
      types: [FoodType.popular]),
  Food(
      id: 4,
      picturePath:
          'https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2020/02/makanan-korea-halal.jpg',
      name: 'soto import',
      ingredients: 'ayam jepang, tomat brazil, mie itali, pizza itali',
      description: 'makanan khas dari indonesia yg membuat dompet anda bergoyang',
      price: 150000,
      rate: 4.5,
      types: [FoodType.recomended]),
  Food(
      id: 5,
      picturePath:
          'https://asset.kompas.com/crops/WYq4XN80g75EJS0B6vdJ0G4z2OQ=/0x0:739x493/750x500/data/photo/2020/04/24/5ea2e1438c283.jpg',
      name: 'nasi padang',
      ingredients: 'daging sapi jepang, kelapa pilihan, rempah rempah, bawang merah, bawang putih',
      description: 'makanan khas sumatra barat yg makanan number 1',
      price: 12000,
      rate: 4.3,
      types: [FoodType.popular])
];
