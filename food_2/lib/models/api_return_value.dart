part of 'models.dart';

class ApiReturnValue<T> {
  final T value; //kalau benar maka akan mengembalikan value/nilai
  final String message; //kalau salah maka akan mengembalikan message/pesan

  ApiReturnValue({this.value, this.message});
}

