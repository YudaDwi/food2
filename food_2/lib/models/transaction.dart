part of 'models.dart';

enum TransactionStatus { delivered, on_delivery, pending, canceled }

class Transaction extends Equatable {
  final int id;
  final Food food;
  final int quantity;
  final int total;
  final DateTime dateTime;
  final TransactionStatus status;
  final User user;
  final String paymentUrl;

  Transaction(
      {this.id,
      this.food,
      this.quantity,
      this.total,
      this.dateTime,
      this.status,
      this.user,
      this.paymentUrl});

  factory Transaction.fromJson(Map<String, dynamic> data) => Transaction(
      id: data['id'],
      //hasil dari konversi data dgn key food,
      //lalu kita konversi dari fromJson ke Food
      food: Food.fromJson(data['food']),
      quantity: data['quantity'],
      total: data['total'],
      //jadi pada key created_at itu value nya from milisecondsinceepoch
      //millisecondsinceepoch mungkin itu hituangan dari UTC
      dateTime: DateTime.fromMillisecondsSinceEpoch(data['created_at']),
      //dan status itu memang kembaliannya berupa string
      status: (data['status'] == 'PENDING')
          ? (TransactionStatus.delivered)
          : (data['status'] == 'CANCELED')
              ? TransactionStatus.canceled
              : TransactionStatus.on_delivery,
      paymentUrl: data['payment_url']);

  //transaction copywith cara kerja nya mirip dgn text.copywith
  //jadi biar bisa di edit lagi
  Transaction copywith(
      {int id,
      Food food,
      int quantity,
      DateTime dateTime,
      TransactionStatus status,
      int total,
      User user,
      String paymentUrl}) {
    //dan disini di cek kalau id nya kosong maka akan diisini id ini/yg baru
    return Transaction(
        id: id ?? this.id,
        food: food ?? this.food,
        quantity: quantity ?? this.quantity,
        total: total ?? this.total,
        dateTime: dateTime ?? this.dateTime,
        status: status ?? this.status,
        user: user ?? this.user,
        paymentUrl: paymentUrl ?? this.paymentUrl);
  }

  @override
  List<Object> get props => [id, food, quantity, total, dateTime, status, user];
}

List<Transaction> transaction = [
  Transaction(
      id: 1,
      food: mockFoods[1],
      quantity: 10,
      //1.13 harga makanan dikali quantity/jumlah pesanan  diklai pajak + 50 k itu ongkos kirim
      total: (mockFoods[1].price * 10 * 1.1).round() + 50000,
      dateTime: DateTime.now(),
      status: TransactionStatus.on_delivery,
      user: mockUser),
  Transaction(
      id: 2,
      food: mockFoods[2],
      quantity: 7,
      total: (mockFoods[2].price * 10 * 1.1).round() + 50000,
      dateTime: DateTime.now(),
      status: TransactionStatus.delivered,
      user: mockUser),
  Transaction(
      id: 3,
      food: mockFoods[3],
      quantity: 5,
      total: (mockFoods[3].price * 10 * 1.1).round() + 50000,
      dateTime: DateTime.now(),
      status: TransactionStatus.canceled,
      user: mockUser)
];
