part of 'models.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String email;
  final String picturePath;
  final String phoneNum;
  final String address;
  final String houseNum;
  final String city;
  static String token;
  //5.2 kok kayake gak perlu constructor

  User(
      {this.id,
      this.name,
      this.email,
      this.picturePath,
      this.phoneNum,
      this.houseNum,
      this.address,
      this.city});

  factory User.fromJson(Map<String, dynamic> data) => User(
      id: data['id'],
      name: data['name'],
      email: data['email'],
      address: data['address'],
      houseNum: data['houseNumber'],
      phoneNum: data['phoneNumber'],
      city: data['city'],
      picturePath: data['profile_photo_url']);

  //5.2 membuat method copywith yaitu membuat objek baru mirip dgn transaction copywith
  User copyWith(
          {int id,
          String name,
          String email,
          String address,
          String picturePath,
          String phoneNum,
          String houseNum,
          String city}) =>
      User(
          //id dari copywith di cek kalau ada dia akan diisi oleh id copywith tapi kalau tidak ada
          //dia diisi this.id/id dari usermodel asli
          id: id ?? this.id,
          name: name ?? this.name,
          email: email ?? this.email,
          address: address ?? this.address,
          picturePath: picturePath ?? this.picturePath,
          phoneNum: phoneNum ?? this.phoneNum,
          houseNum: houseNum ?? this.houseNum,
          city: city ?? this.city);

  @override
  List<Object> get props => [
        id,
        name,
        email,
        picturePath,
        phoneNum,
        picturePath,
        houseNum,
        city,
        address
      ];
}

User mockUser = User(
    id: 1,
    name: 'yuda dwi hardianto',
    email: 'yudadwi0@gmail.com',
    picturePath:
        'https://res.cloudinary.com/dk0z4ums3/image/upload/v1594169572/attached_image/daftar-makanan-tinggi-garam-yang-perlu-diwaspadai.jpg',
    phoneNum: '0852 04912761',
    houseNum: '91',
    city: 'ponorogo',
    address: 'ngrupit jenangan');
