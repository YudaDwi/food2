part of 'user_cubit.dart';

@immutable
abstract class UserState extends Equatable {}

class UserInitial extends UserState {
  @override
  List<Object> get props => [];
}

class UserLoaded extends UserState {
  final User user;
  UserLoaded(this.user);
  @override
  List<Object> get props => [user];
}

class UserLoadedFailed extends UserState {
  final String message;
  UserLoadedFailed(this.message);

  @override
  List<Object> get props => [message];
}
