part of 'transaction_cubit.dart';

@immutable
abstract class TransactionState extends Equatable {}

class TransactionInitial extends TransactionState {
  @override
  List<Object> get props => [];
}

class TransactionLoaded extends TransactionState {
  final List<Transaction> transaction;
  TransactionLoaded(this.transaction);

  @override
  List<Object> get props => [transaction];
}

class TransactionLoadedFailed extends TransactionState {
  final String message;
  TransactionLoadedFailed(this.message);

  @override
  List<Object> get props => [message];
}
