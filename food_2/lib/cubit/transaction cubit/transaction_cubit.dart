import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:food_2/models/models.dart';
import 'package:food_2/services/services.dart';
import 'package:meta/meta.dart';

part 'transaction_state.dart';

class TransactionCubit extends Cubit<TransactionState> {
  TransactionCubit() : super(TransactionInitial());

  Future<void> getTransaction() async {
    ApiReturnValue<List<Transaction>> result =
        await TransactionServices.getTransaction();

    if (result.value != null) {
      emit(TransactionLoaded(result.value));
    } else {
      emit(TransactionLoadedFailed(result.message));
    }
  }

  //5.8 kenapa diganti menjadi string karena nanti kembalian nya akan diarahkan ke midtrans nya
  Future<String> submitTransction(Transaction transaction) async {
    ApiReturnValue<Transaction> result =
        await TransactionServices.submitTransaction(transaction);

    if (result.value != null) {
      emit(TransactionLoaded(
          //state saat ini digabungkan dgn hasil dari result value yg dikembalikan
          (state as TransactionLoaded).transaction + [result.value]));
      return result.value.paymentUrl;
    } else {
      return null;
    }
  }
}


//karena emang dibagian transaksi beda daripada food maupun user