part of 'food_cubit.dart';

@immutable
abstract class FoodState extends Equatable {}

class FoodInitial extends FoodState {
  @override
  List<Object> get props => [];
}

class FoodLoaded extends FoodState {
  //dan ini seperti api return value
  //ini untuk value dan nanti untuk message/failed
  final List<Food> food;

  FoodLoaded(this.food);
  @override
  List<Object> get props => [food];
}

class FoodLoadedFailed extends FoodState {
  final String message;
  FoodLoadedFailed(this.message);

  @override
  List<Object> get props => [this.message];
}
